# - Try to find CSYNC
# Once done this will define
#
#  CSYNC_FOUND - system has CSYNC
#  CSYNC_INCLUDE_DIRS - the CSYNC include directory
#  CSYNC_LIBRARIES - Link these to use CSYNC
#  CSYNC_DEFINITIONS - Compiler switches required for using CSYNC
#
#  Copyright (c) 2008 Andreas Schneider <mail�@cynapses.org>
#
#  Redistribution and use is allowed according to the terms of the New
#  BSD license.
#  For details see the accompanying COPYING-CMAKE-SCRIPTS file.
#


if (CSYNC_LIBRARIES AND CSYNC_INCLUDE_DIRS)
  # in cache already
  set(CSYNC_FOUND TRUE)
else (CSYNC_LIBRARIES AND CSYNC_INCLUDE_DIRS)
  find_path(CSYNC_INCLUDE_DIR
    NAMES
      csync/csync.h
    PATHS
      /usr/include
      /usr/local/include
      /opt/local/include
      /sw/include
  )

  find_library(CSYNC_LIBRARY
    NAMES
      csync
    PATHS
      /usr/lib
      /usr/local/lib
      /opt/local/lib
      /sw/lib
  )

  if (CSYNC_LIBRARY)
    set(CSYNC_FOUND TRUE)
  endif (CSYNC_LIBRARY)

  set(CSYNC_INCLUDE_DIRS
    ${CSYNC_INCLUDE_DIR}
  )

  if (CSYNC_FOUND)
    set(CSYNC_LIBRARIES
      ${CSYNC_LIBRARIES}
      ${CSYNC_LIBRARY}
    )
  endif (CSYNC_FOUND)

  if (CSYNC_INCLUDE_DIRS AND CSYNC_LIBRARIES)
     set(CSYNC_FOUND TRUE)
  endif (CSYNC_INCLUDE_DIRS AND CSYNC_LIBRARIES)

  if (CSYNC_FOUND)
    if (NOT CSYNC_FIND_QUIETLY)
      message(STATUS "Found CSYNC: ${CSYNC_LIBRARIES}")
    endif (NOT CSYNC_FIND_QUIETLY)
  else (CSYNC_FOUND)
    if (CSYNC_FIND_REQUIRED)
      message(FATAL_ERROR "Could not find CSYNC")
    endif (CSYNC_FIND_REQUIRED)
  endif (CSYNC_FOUND)

  # show the CSYNC_INCLUDE_DIRS and CSYNC_LIBRARIES variables only in the advanced view
  mark_as_advanced(CSYNC_INCLUDE_DIRS CSYNC_LIBRARIES)

endif (CSYNC_LIBRARIES AND CSYNC_INCLUDE_DIRS)

