include(CheckCSourceCompiles)

check_c_source_compiles("
#define __unused __attribute__((unused))

static int do_nothing(int i __unused)
{
    return 0;
}

int main(void)
{
    int i;

    i = do_nothing(5);
    if (i > 5) {
        return 1;
    }

    return 0;
}" HAVE_UNUSED_ATTRIBUTE)
