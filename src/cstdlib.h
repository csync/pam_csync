/*
 * pam_csync -- a module to support roaming home directories with csync
 *
 * Copyright (c) 2008      by Andreas Schneider <mail@cynapses.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * vim: ts=2 sw=2 et cindent
 */

#ifndef _CSTDLIB_H
#define _CSTDLIB_H

#include <stdlib.h>
#include <string.h>

#ifdef malloc
#undef malloc
#endif
#define malloc(x) DO_NOT_CALL_MALLOC__USE_C_MALLOC_INSTEAD

#ifdef calloc
#undef calloc
#endif
#define calloc(x,y) DO_NOT_CALL_CALLOC__USE_C_CALLOC_INSTEAD

#ifdef realloc
#undef realloc
#endif
#define realloc(x,y) DO_NOT_CALL_REALLOC__USE_C_REALLOC_INSTEAD

#ifdef dirname
#undef dirname
#endif
#define dirname(x) DO_NOT_CALL_MALLOC__USE_C_DIRNAME_INSTEAD

#ifdef strdup
#undef strdup
#endif
#define strdup(x) DO_NOT_CALL_STRDUP__USE_C_STRDUP_INSTEAD

#define INT_TO_POINTER(i) (void *) i
#define POINTER_TO_INT(p) *((int *) (p))

/** Zero a structure */
#define ZERO_STRUCT(x) memset((char *)&(x), 0, sizeof(x))

/** Zero a structure given a pointer to the structure */
#define ZERO_STRUCTP(x) do { if ((x) != NULL) memset((char *)(x), 0, sizeof(*(x))); } while(0)

/** Free memory and zero the pointer */
#define SAFE_FREE(x) do { if ((x) != NULL) {free(x); x=NULL;} } while(0)

/**
 * @brief Allocates memory for an array.
 *
 * Allocates memory for an array of <count> elements of <size> bytes each and
 * returns a pointer to the allocated memory. The memory is set to zero.
 *
 * @param count   Amount of elements to allocate
 * @param size    Size in bytes of each element to allocate.
 *
 * @return A unique pointer value that can later be successfully passed to
 *         free(). If size or count is 0, NULL will be returned.
 */
void *c_calloc(size_t count, size_t size);

/**
 * @brief Allocates memory for an array.
 *
 * Allocates <size> bytes of memory. The memory is set to zero.
 *
 * @param size    Size in bytes to allocate.
 *
 * @return A unique pointer value that can later be successfully passed to
 *         free(). If size or count is 0, NULL will be returned.
 */
void *c_malloc(size_t size);

/**
 * @brief Changes the size of the memory block pointed to.
 *
 * Newly allocated memory will be uninitialized.
 *
 * @param ptr   Pointer to the memory which should be resized.
 * @param size  Value to resize.
 *
 * @return If ptr is NULL, the call is equivalent to c_malloc(size); if size
 *         is equal to zero, the call is equivalent to free(ptr). Unless ptr
 *         is NULL, it must have been returned by an earlier call to
 *         c_malloc(), c_calloc() or c_realloc(). If the area pointed to was
 *         moved, a free(ptr) is done.
 */
void *c_realloc(void *ptr, size_t size);

/**
 * @brief Duplicate a string.
 *
 * The function returns a pointer to a newly allocated string which is a
 * duplicate of the string str.
 *
 * @param str   String to duplicate.
 *
 * @return Returns a pointer to the duplicated string, or NULL if insufficient
 * memory was available.
 *
 */
char *c_strdup(const char *str);

/**
 * @brief Duplicate a string.
 *
 * The function returns a pointer to a newly allocated string which is a
 * duplicate of the string str of size bytes.
 *
 * @param str   String to duplicate.
 *
 * @param size  Size of the string to duplicate.
 *
 * @return Returns a pointer to the duplicated string, or NULL if insufficient
 * memory was available. A terminating null byte '\0' is added.
 *
 */
char *c_strndup(const char *str, size_t size);

/**
 * @breif Replace a string with another string in a source string.
 *
 * @param src      String to search for pattern.
 *
 * @param pattern  Pattern to search for in the source string.
 *
 * @param repl     The string which which should replace pattern if found.
 *
 * @return  Return a pointer to the source string.
 */
char *c_strreplace(const char *src, const char *pattern, const char *replace);

#endif /* _CSTDLIB_H */
