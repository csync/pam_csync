/*
 * pam_csync -- a module to support roaming home directories with csync
 *
 * Copyright (c) 2008      by Andreas Schneider <mail@cynapses.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * vim: ts=2 sw=2 et cindent
 */

#include "cstdlib.h"

void *c_calloc(size_t count, size_t size) {
  if (size == 0 || count == 0) {
    return NULL;
  }
#undef calloc
  return calloc(count, size);
#define calloc(x,y) DO_NOT_CALL_CALLOC__USE_XCALLOC_INSTEAD
}

void *c_malloc(size_t size) {
  if (size == 0) {
    return NULL;
  }
#undef malloc
  return c_calloc(1, size);
#define malloc(x) DO_NOT_CALL_MALLOC__USE_XMALLOC_INSTEAD
}

void *c_realloc(void *ptr, size_t size) {
#undef realloc
  return realloc(ptr, size);
#define realloc(x,y) DO_NOT_CALL_REALLOC__USE_XREALLOC_INSTEAD
}

char *c_strdup(const char *str) {
  char *ret;
  if (str == NULL) {
    return NULL;
  }
  ret = (char *) c_malloc(strlen(str) + 1);
  strcpy(ret, str);
  return ret;
}

char *c_strndup(const char *str, size_t size) {
  char *ret;
  size_t len;
  if (str == NULL) {
    return NULL;
  }
  len = strlen(str);
  if (len > size) {
    len = size;
  }
  ret = (char *) c_malloc(len + 1);
  strncpy(ret, str, len);
  ret[size] = '\0';
  return ret;
}

char *c_strreplace(const char *src, const char *pattern, const char *replace)
{
    char *p = NULL;
    char *src_replaced = NULL;

    if (src == NULL) {
        return NULL;
    }

    if (pattern == NULL || replace == NULL) {
        return c_strdup(src);
    }

    p = strstr(src, pattern);

    if (p != NULL) {
        size_t offset = p - src;
        size_t pattern_len = strlen(pattern);
        size_t replace_len = strlen(replace);
        size_t len  = strlen(src);
        size_t len_replaced = len + replace_len - pattern_len + 1;

        src_replaced = (char *)c_malloc(len_replaced);

        if (src_replaced == NULL) {
            return NULL;
        }

        memset(src_replaced, 0, len_replaced);
        memcpy(src_replaced, src, offset);
        memcpy(src_replaced + offset, replace, replace_len);
        memcpy(src_replaced + offset + replace_len, src + offset + pattern_len, len - offset - pattern_len);
        return src_replaced; /* free in the caller */
    } else {
        return c_strdup(src);
    }
}
