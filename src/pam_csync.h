/*
 * pam_csync -- a module to support roaming home directories with csync
 *
 * Copyright (c) 2000      by Elvis Pfützenreuter <epx@conectiva.com>
 * Copyright (c) 2005-2008 by Jan Engelhardt
 * Copyright (c) 2005      by Bastian Kleineidam
 * Copyright (c) 2008      by Andreas Schneider <mail@cynapses.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * vim: ts=2 sw=2 et cindent
 */

#ifndef _PAM_CSYNC_H
#define _PAM_CSYNC_H

#define PAM_CSYNC_CONFIG_FILE "/etc/security/pam_csync.conf"

#define PAM_CSYNC_MAX_PWLEN 127

#define PAM_CSYNC_AUTHTOK       "PAM_CSYNC_AUTHTOK"

#define PAM_WINBIND_PROFILEPATH "PAM_WINBIND_PROFILEPATH"
#define PAM_WINBIND_HOMEDIR     "PAM_WINBIND_HOMEDIR"

#define _on(x, y) (x & y)
#define _off(x, y) (!(x & y))

enum auth_type_e {
  GET_PASS,
  USE_FIRST_PASS,
  TRY_FIRST_PASS,
  SOFT_TRY_PASS,
};

typedef struct args_s {
  enum auth_type_e auth_type;
  int nullok;
  int debug;
  int silent;
} args_t;

#endif /* _PAM_CSYNC_H */
