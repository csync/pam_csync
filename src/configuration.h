/*
 * pam_csync -- a module to support roaming home directories with csync
 *
 * Copyright (c) 2008      by Andreas Schneider <mail@cynapses.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * vim: ts=2 sw=2 et cindent
 */

#ifndef _P_CONIFG_H
#define _P_CONIFG_H

typedef struct config_s {
  int krb5_set;
  char *uri;
  char *user;
  char *msg_authpw;
  char *msg_sessionpw;
  struct {
    char **vector;
    int count;
  } exclude;
} config_t;

extern void config_init(config_t *c);
extern int config_load(const char *f, config_t *c);
extern void config_free(config_t *c);

#endif /* _P_CONIFG_H */
