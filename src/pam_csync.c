/*
 * pam_csync -- a module to support roaming home directories with csync
 *
 * Copyright (c) 2000      by Elvis Pfützenreuter <epx@conectiva.com>
 * Copyright (c) 2005-2008 by Jan Engelhardt
 * Copyright (c) 2005      by Bastian Kleineidam
 * Copyright (c) 2008      by Andreas Schneider <mail@cynapses.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * vim: ts=2 sw=2 et cindent
 */

#include "config.h"

/*
 * This file is mostly based on pam_mount.c
 */
#include <assert.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <stdarg.h>
#include <fcntl.h>
#include <limits.h>
#include <unistd.h>
#include <pwd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define PAM_SM_AUTH 1
#define PAM_SM_PASSWORD 1
#define PAM_SM_SESSION 1

#include <security/pam_modules.h>
#include <security/_pam_macros.h>
#include <security/pam_modutil.h>
#include <security/pam_ext.h>

#include <csync/csync.h>

#include "pam_csync.h"
#include "configuration.h"
#include "cstdlib.h"
#include "util.h"

#ifdef HAVE_UNUSED_ATTRIBUTE
#define __unused __attribute__((unused))
#else
#define __unused
#endif

extern unsigned int pc_debug;
extern unsigned int pc_silent;

config_t pc_config;
args_t pc_args;

static const char *_pam_error_code_str(int err) {
  switch (err) {
    case PAM_SUCCESS:
      return "PAM_SUCCESS";
    case PAM_OPEN_ERR:
      return "PAM_OPEN_ERR";
    case PAM_SYMBOL_ERR:
      return "PAM_SYMBOL_ERR";
    case PAM_SERVICE_ERR:
      return "PAM_SERVICE_ERR";
    case PAM_SYSTEM_ERR:
      return "PAM_SYSTEM_ERR";
    case PAM_BUF_ERR:
      return "PAM_BUF_ERR";
    case PAM_PERM_DENIED:
      return "PAM_PERM_DENIED";
    case PAM_AUTH_ERR:
      return "PAM_AUTH_ERR";
    case PAM_CRED_INSUFFICIENT:
      return "PAM_CRED_INSUFFICIENT";
    case PAM_AUTHINFO_UNAVAIL:
      return "PAM_AUTHINFO_UNAVAIL";
    case PAM_USER_UNKNOWN:
      return "PAM_USER_UNKNOWN";
    case PAM_MAXTRIES:
      return "PAM_MAXTRIES";
    case PAM_NEW_AUTHTOK_REQD:
      return "PAM_NEW_AUTHTOK_REQD";
    case PAM_ACCT_EXPIRED:
      return "PAM_ACCT_EXPIRED";
    case PAM_SESSION_ERR:
      return "PAM_SESSION_ERR";
    case PAM_CRED_UNAVAIL:
      return "PAM_CRED_UNAVAIL";
    case PAM_CRED_EXPIRED:
      return "PAM_CRED_EXPIRED";
    case PAM_CRED_ERR:
      return "PAM_CRED_ERR";
    case PAM_NO_MODULE_DATA:
      return "PAM_NO_MODULE_DATA";
    case PAM_CONV_ERR:
      return "PAM_CONV_ERR";
    case PAM_AUTHTOK_ERR:
      return "PAM_AUTHTOK_ERR";
    case PAM_AUTHTOK_RECOVERY_ERR:
      return "PAM_AUTHTOK_RECOVERY_ERR";
    case PAM_AUTHTOK_LOCK_BUSY:
      return "PAM_AUTHTOK_LOCK_BUSY";
    case PAM_AUTHTOK_DISABLE_AGING:
      return "PAM_AUTHTOK_DISABLE_AGING";
    case PAM_TRY_AGAIN:
      return "PAM_TRY_AGAIN";
    case PAM_IGNORE:
      return "PAM_IGNORE";
    case PAM_ABORT:
      return "PAM_ABORT";
    case PAM_AUTHTOK_EXPIRED:
      return "PAM_AUTHTOK_EXPIRED";
    case PAM_MODULE_UNKNOWN:
      return "PAM_MODULE_UNKNOWN";
    case PAM_BAD_ITEM:
      return "PAM_BAD_ITEM";
    case PAM_CONV_AGAIN:
      return "PAM_CONV_AGAIN";
    case PAM_INCOMPLETE:
      return "PAM_INCOMPLETE";
    default:
      return NULL;
  }
}

static void pc_enter_function_log(pam_handle_t *pamh, const char *function) {
  const void *item = NULL;
  const char *service = "unkown";

  pam_get_item(pamh, PAM_SERVICE, &item);
  if (item != NULL) {
    service = (const char *) item;
  }

  pc_log("ENTER: %s called by %s, pamh: %p, pid: %d\n",
      function, service, pamh, getpid());
}

static void pc_leave_function_log(pam_handle_t *pamh, const char *function,
    int retval) {
  const void *item = NULL;
  const char *service = "unkown";

  pam_get_item(pamh, PAM_SERVICE, &item);
  if (item != NULL) {
    service = (const char *) item;
  }

  pc_log("LEAVE: %s called by %s, retval: %s (%d)\n",
      function, service, _pam_error_code_str(retval), retval);
}

/* cleanup functions */
static void _cleanup_authtok(pam_handle_t *pamh, void *data, int err) {
  assert(pamh != NULL);
  (void) pamh;

  pc_warn("cleanup authtok (%d)\n", err);

  if (data != NULL) {
    memset(data, 0, strlen(data));
    munlock(data, strlen(data) + 1);
    SAFE_FREE(data);
  }

  return;
}

static void _cleanup_config(pam_handle_t *pamh, void *data, int err) {
  assert(pamh != NULL);
  (void) pamh;

  pc_warn("cleanup config (%d)\n", err);

  if (data != NULL) {
    config_free(data);
  }

  return;
}

/* parse arguments */
static int _pam_parse(const pam_handle_t *pamh, int flags, int argc,
                      const char **argv) {
  int i = 0;

  assert(pamh != NULL);
  (void) pamh;

  ZERO_STRUCT(pc_args);

  assert(argc >= 0);
  for (i = 0; i < argc; i++) {
    assert(argv[i] != NULL);
  }

  if (flags & PAM_SILENT) {
    pc_args.silent = 1;
  }

  pc_args.auth_type = GET_PASS;

  /* step through arguments */
  for (i = 0; i < argc; ++i) {
    /* generic options */
    if (strcmp("use_first_pass", argv[i]) == 0)
      pc_args.auth_type = USE_FIRST_PASS;
    else if (strcmp("try_first_pass", argv[i]) == 0)
      pc_args.auth_type = TRY_FIRST_PASS;
    else if (strcmp("soft_try_pass", argv[i]) == 0)
      pc_args.auth_type = SOFT_TRY_PASS;
    else if (strcmp("nullok", argv[i]) == 0)
      pc_args.nullok = 1;
    else if (strcmp("silent", argv[i]) == 0)
      pc_silent = 1;
    else if (strcmp("debug", argv[i]) == 0) {
      pc_debug = 1;
    } else {
      pc_warn("unknown option: %s\n", argv[i]);
      return -1;
    }
  }

  return 0;
}

/*
 * These are the common calls of authenticate and open_session
 */
static int _pam_init(pam_handle_t *pamh, int flags, int argc,
    const char **argv) {

  int rc = 0;
  const char *pam_user;

  ZERO_STRUCT(pc_config);
  config_init(&pc_config);

  _pam_parse(pamh, flags, argc, argv);

  /*
   * call pam_get_user again because ssh calls PAM fns from seperate
   * processes.
   */
  rc = pam_get_user(pamh, &pam_user, NULL);
  if (rc != PAM_SUCCESS) {
    pc_log("ERROR: could not get user\n");
    /*
     * do NOT return PAM_SERVICE_ERR or root will not be able to
     * su to other users.
     */
    return PAM_SUCCESS;
  }

  if (config_load(PAM_CSYNC_CONFIG_FILE, &pc_config) < 0) {
    return PAM_SERVICE_ERR;
  }

  pc_config.user = c_strdup(pam_user);

  return -1;
}

/*
 * Note: Adapted from pam_unix/support.c.
 */
static int _pam_converse(pam_handle_t *pamh, int nargs,
    const struct pam_message **message, struct pam_response **resp) {
  int rc;
  const void *item = NULL;
  struct pam_conv *conv = NULL;

  assert(pamh != NULL);
  assert(nargs >= 0);
  assert(resp != NULL);

  *resp = NULL;

  rc = pam_get_item(pamh, PAM_CONV, &item);

  /* This could happen in some strange cases */
  if (item == NULL && rc == PAM_SUCCESS) {
    rc = PAM_SYSTEM_ERR;
  }

  if (rc == PAM_SUCCESS) {
    conv = (struct pam_conv *) item;
    rc = conv->conv(nargs, message, resp, conv->appdata_ptr);
    if (rc != PAM_SUCCESS) {
      pc_log("ERROR: conversation failure: %s\n",
          pam_strerror(pamh, rc));
    }
  } else if (rc != PAM_CONV_AGAIN) {
    pc_log("ERROR: could not obtain conversation fucntions: %s\n",
        pam_strerror(pamh, rc));
  }

  return rc;
}

/*
 * Note: Adapted from pam_unix/support.c:_unix_pam_read_password().
 */
static int _pam_read_password(pam_handle_t *pamh, const char *prompt,
    char **pass) {
  int rc;
  struct pam_message msg;
  const struct pam_message *pmsg = &msg;
  struct pam_response *resp = NULL;

  assert(pamh != NULL);
  assert(pass != NULL);

  pc_log("INFO: entering _pam_read_password\n");

  *pass = NULL;
  msg.msg_style = PAM_PROMPT_ECHO_OFF;
  msg.msg = (prompt == NULL) ? "Password: " : prompt;

  rc  = _pam_converse(pamh, 1, &pmsg, &resp);

  if (rc == PAM_SUCCESS) {
    *pass = c_strdup(resp->resp);
  }

  assert(rc != PAM_SUCCESS || (pass != NULL && *pass != NULL));

  return rc;
}

/* run csync */
static int _csync(const char *user, const char *pwd,
    const char *source, const char *dest) {
  int status = PAM_SUCCESS;
  pid_t childpid = 0;
  CSYNC *csync = NULL;
  char *new_dest = NULL;
  char cwd[PATH_MAX] = {0};

  assert(user != NULL);
  pc_log("INFO: calling csync %s <-> %s\n", source, dest);

  new_dest = c_strdup(dest);
  if (new_dest == NULL) {
    status = PAM_SYSTEM_ERR;
    goto err;
  }

  if (pwd) {
    new_dest = c_strreplace(new_dest, "%(PASS)", pwd);
    if (new_dest == NULL) {
      pc_log("ERROR: expanding password\n");
      status = PAM_SYSTEM_ERR;
      goto err;
    }
  }

  /* now create new process */
  childpid = fork();

  /* fork succeeded */
  if (childpid >= 0) {
    /* fork() returns 0 to the child process */
    if (childpid == 0) {
      int rc = PAM_SYSTEM_ERR;

      pc_log("INFO: child pid running csync: %d\n", getpid());

      pc_setuid(user);
      pc_print_ids("child running with ids");

      if (chdir(source) < 0) {
        pc_warn("chhdir failed\n");
      }
      pc_log("INFO: child pwd: %s\n", getcwd(cwd, PATH_MAX - 1));

      rc = csync_create(&csync, source, new_dest);
      if (rc < 0) {
        pc_warn("unable to create csync context.\n");
        rc = PAM_SYSTEM_ERR;
        goto out;
      }

      pc_log("INFO: initializing csync.\n");
      rc = csync_init(csync);
      if (rc < 0) {
        pc_warn("ERROR: unable to initialize csync.\n");
        rc = PAM_SUCCESS;
        goto out;
      }

      pc_log("INFO: running update detection.\n");
      rc = csync_update(csync);
      if (rc < 0) {
        pc_warn("ERROR: update detection failed.\n");
        rc = PAM_SUCCESS;
        goto out;
      }

      pc_log("INFO: running reconciliation.\n");
      rc = csync_reconcile(csync);
      if (rc < 0) {
        pc_warn("ERROR: reconciliation failed.\n");
        rc = PAM_SUCCESS;
        goto out;
      }

      pc_log("INFO: running propagation.\n");
      rc = csync_propagate(csync);
      if (rc < 0) {
        pc_warn("ERROR: propagation failed.\n");
        rc = PAM_SUCCESS;
        goto out;
      }

      rc = PAM_SUCCESS;
out:
      csync_destroy(csync);
      exit(rc);
    } else {
      wait(&status);

      pc_log("INFO: child's exit status: %d\n",
          WEXITSTATUS(status));

      if (WIFSIGNALED(status)) {
        pc_warn("INFO: child's term signal: %d\n",
            WTERMSIG(status));
      }
    }
  /* fork returns -1 on failure */
  } else {
    pc_warn("ERROR: fork failed: %s\n", strerror(errno));
    status = PAM_SYSTEM_ERR;
  }
err:
  memset(new_dest, 0, strlen(new_dest));
  SAFE_FREE(new_dest);

  return WEXITSTATUS(status);
}

/* pam authentication management */

/*
 * The user's system password is added to PAM's global module data. This is
 * because pam_sm_open_session() does not allow access to the user's password.
 */
PAM_EXTERN int pam_sm_authenticate(pam_handle_t *pamh, int flags,
    int argc, const char **argv) {
  int rc = PAM_SUCCESS;
  const void *tmp = NULL;
  char *authtok = NULL;

  (void) tmp;
  assert(pamh != NULL);

  pc_enter_function_log(pamh, "pam_sm_authenticate");

  rc = _pam_init(pamh, flags, argc, argv);
  if (rc != -1) {
    return rc;
  }

  /* get password from PAM system */
  if (pc_args.auth_type != GET_PASS) {
    const void *item = NULL;
    const char *ptr = NULL;

    rc = pam_get_item(pamh, PAM_AUTHTOK, &item);
    ptr = (const char *) item;
    if (rc != PAM_SUCCESS || ptr == NULL) {
      if (rc == PAM_SUCCESS && ptr == NULL && ! pc_args.nullok) {
        rc = PAM_AUTHINFO_UNAVAIL;
      }
      pc_log("ERROR: could not get password from PAM system\n");
      if (pc_args.auth_type == USE_FIRST_PASS) {
        rc = PAM_AUTHINFO_UNAVAIL;
        goto out;
      }
    } else {
      authtok = c_strdup(ptr);
    }
  }

  /* get password directly */
  if (authtok == NULL) {
    if (pc_args.auth_type == SOFT_TRY_PASS) {
      rc = PAM_SUCCESS;
      goto out;
    }

    rc = _pam_read_password(pamh, pc_config.msg_authpw, &authtok);
    if (rc != PAM_SUCCESS) {
      pc_log("ERROR: trying to read password\n");
      goto out;
    }

    /* pam_set_item() copies to PAM-internal memory */
    rc = pam_set_item(pamh, PAM_AUTHTOK, authtok);
    if (rc != PAM_SUCCESS) {
      pc_log("ERROR: trying to export password\n");
      goto out;
    }
  }

  if (strlen(authtok) > PAM_CSYNC_MAX_PWLEN) {
    pc_warn("ERROR: password too long\n");
    rc = PAM_AUTH_ERR;
    goto out;
  }

  pc_log("INFO: saving authtok for session code\n");
  rc = pam_set_data(pamh, PAM_CSYNC_AUTHTOK, authtok, _cleanup_authtok);
  if (rc != PAM_SUCCESS) {
    pc_warn("ERROR: trying to save authtok for session code\n");
    goto out;
  }

  if (mlock(authtok, strlen(authtok) + 1) < 0) {
    pc_warn("ERROR: authtok %s\n", strerror(errno));
    rc = PAM_AUTH_ERR;
    goto out;
  }

out:
  pc_leave_function_log(pamh, "pam_sm_authenticate", rc);

  return rc;
}

PAM_EXTERN int pam_sm_setcred(pam_handle_t *pamh,
                              int flags,
                              int argc __unused,
                              const char **argv __unused)
{

  int rc = PAM_SUCCESS;

  pc_enter_function_log(pamh, "pam_sm_setcred");

  pc_log("INFO: pam_sm_setcred (%s) called\n",
      (flags & PAM_ESTABLISH_CRED) ? "establish credential" :
      (flags & PAM_REINITIALIZE_CRED) ? "reinitialize credential" :
      (flags & PAM_REFRESH_CRED) ? "refresh credential" :
      (flags & PAM_DELETE_CRED) ? "delete credential" : "unknown flag");

  pc_leave_function_log(pamh, "pam_sm_setcred", rc);

  return rc;
}

/*
 * This is a placeholder function so PAM does not get mad.
 */
PAM_EXTERN int pam_sm_acct_mgmt(pam_handle_t *pamh __unused,
                                int flags __unused,
                                int argc __unused,
                                const char **argv __unused)
{
  return PAM_IGNORE;
}

PAM_EXTERN int pam_sm_open_session(pam_handle_t *pamh, int flags, int argc,
    const char **argv) {
  int rc = PAM_SUCCESS;
  int val = PAM_SUCCESS;
  int count = 0;
  char *authtok = NULL;
  const char *ccache = NULL;
  char *pam_home = NULL;
  const void *tmp = NULL;

  assert(pamh != NULL);

  rc = _pam_init(pamh, flags, argc, argv);
  if (rc != -1) {
    return rc;
  }

  pc_enter_function_log(pamh, "pam_sm_open_session");

  if (pc_user_excluded(&pc_config)) {
    pc_log("INFO: user %s excluded from synchronization\n", pc_config.user);
    rc = PAM_SUCCESS;
    goto out;
  }

  /* Get the Kerberos CCNAME so we can make it available to csync. */
  ccache = pam_getenv(pamh, "KRB5CCNAME");
  if (ccache == NULL) {
    pc_config.krb5_set = 0;
  } else {
    if (setenv("KRB5CCNAME", ccache, 1) < 0) {
      pc_log("ERROR: KRB5CCNAME setenv failed\n");
    } else {
      pc_log("INFO: KRB5CCNAME=%s\n", getenv("KRB5CCNAME"));
      pc_config.krb5_set = 1;
    }
  }
  pc_log("INFO: kerberos support is %s\n", pc_config.krb5_set ? "enabled" : "disabled");

  /* Store initialized config as PAM data */
  val = pam_get_data(pamh, "pam_csync_config", &tmp);
  if (val == PAM_NO_MODULE_DATA) {
    rc = pam_set_data(pamh, "pam_csync_config", &pc_config, _cleanup_config);
    if (rc != PAM_SUCCESS) {
      pc_log("ERROR: trying to save config structure\n");
      goto out;
    }
  }

  /* check uri */
  if (pc_config.uri == NULL) {
    pc_log("ERROR: no uri specified\n");
    goto out;
  }

  /* get the authtok */
  rc = pam_get_data(pamh, PAM_CSYNC_AUTHTOK, &tmp);
  authtok = (char *)tmp;
  if (rc != PAM_SUCCESS) {
    if (pc_args.auth_type == SOFT_TRY_PASS) {
      rc = PAM_SUCCESS;
      goto out;
    }
    pc_log("ERROR: trying to retrieve authtok from auth code\n");
    rc = _pam_read_password(pamh, pc_config.msg_sessionpw, &authtok);
    if (rc != PAM_SUCCESS) {
      pc_log("ERROR: trying to read password %s\n", pam_strerror(pamh, rc));
      goto out;
    }
  }

  pc_print_ids("session open");

  rc = pc_get_home(&pam_home, pc_config.user);
  if (rc != PAM_SUCCESS) {
    pc_log("ERROR: could not get home\n");
    goto out;
  }

  /* get login count */
  count = pc_modify_login_count(pc_config.user, 0);
  if (count == -ESTALE) {
    count = 1;
  } else if (count < 0) {
    rc = PAM_SESSION_ERR;
    goto out;
  }

  if (count == 0) {
    /* if uri is set to winbind, get the path from pam_winbind */
    if (strncmp(pc_config.uri, "winbind", 7) == 0) {
      if (strcmp(pc_config.uri, "winbind") == 0 ||
          strcmp(pc_config.uri, "winbind_profile") == 0) {
        val = pam_get_data(pamh, PAM_WINBIND_PROFILEPATH, &tmp);
      } else if (strcmp(pc_config.uri, "winbind_homedir") == 0) {
        val = pam_get_data(pamh, PAM_WINBIND_HOMEDIR, &tmp);
      } else {
        goto out;
      }

      if (val == PAM_NO_MODULE_DATA) {
        pc_log("ERROR: trying to get %s from winbind\n", pc_config.uri);
        goto out;
      }
      val = pc_winbind_uri(&pc_config.uri, tmp, !pc_config.krb5_set);
      pc_log("INFO: use pam_winbind support; server: %s\n", pc_config.uri);
      if (val < 0) {
        pc_log("ERROR: creating smb uri with winbind profile path\n");
        goto out;
      }
    }

    pc_log("INFO: expanding uri: %s\n", pc_config.uri);
    pc_config.uri = c_strreplace(pc_config.uri, "%(USER)", pc_config.user);
    if (pc_config.uri == NULL) {
      pc_log("ERROR: expanding user\n");
      goto out;
    }

    rc = _csync(pc_config.user, authtok, pam_home, pc_config.uri);
    if (rc != PAM_SUCCESS) {
      pc_log("ERROR: synchronization failed\n");
      goto out;
    }
  }

  /* increment login count */
  count = pc_modify_login_count(pc_config.user, +1);
  if (count == -ESTALE) {
    count = 1;
  } else if (count < 0) {
    rc = PAM_SESSION_ERR;
    goto out;
  }

out:

  pc_leave_function_log(pamh, "pam_sm_open_session", rc);

  SAFE_FREE(pam_home);

  return rc;
}

PAM_EXTERN int pam_sm_close_session(pam_handle_t *pamh,
                                    int flags __unused,
                                    int argc __unused,
                                    const char **argv __unused)
{
  int rc = PAM_SUCCESS;
  int val = PAM_SUCCESS;
  int count = 0;
  const void *tmp = NULL;
  const char *pam_user = NULL;
  char *authtok = NULL;
  char *pam_home = NULL;

  assert(pamh != NULL);

  pc_enter_function_log(pamh, "pam_sm_close_session");

  /*
   * call pam_get_user() again because ssh calls PAM fns from seperate
   * processes.
   */
  rc = pam_get_user(pamh, &pam_user, NULL);
  if (rc != PAM_SUCCESS) {
    pc_log("ERROR: could not get user\n");
    /*
     * do NOT return PAM_SERVICE_ERR or root will not be able to
     * su to other users.
     */
    goto out;
  }

  SAFE_FREE(pc_config.user);
  pc_config.user = c_strdup(pam_user);

  if (pc_user_excluded(&pc_config)) {
    pc_log("INFO: user %s excluded from synchronization\n", pc_config.user);
    rc = PAM_SUCCESS;
    goto out;
  }

  /* get the authtok */
  rc = pam_get_data(pamh, PAM_CSYNC_AUTHTOK, &tmp);
  authtok = (char *) tmp;
  if (rc != PAM_SUCCESS) {
    if (pc_args.auth_type == SOFT_TRY_PASS) {
      rc = PAM_SUCCESS;
      goto out;
    }
    pc_log("ERROR: trying to retrieve authtok from auth code\n");
    rc = _pam_read_password(pamh, pc_config.msg_sessionpw, &authtok);
    if (rc != PAM_SUCCESS) {
      pc_log("ERROR: trying to read password\n");
      goto out;
    }
  }

  rc = pc_get_home(&pam_home, pam_user);
  if (rc != PAM_SUCCESS) {
    pc_log("ERROR: could not get home\n");
    /*
     * do NOT return PAM_SERVICE_ERR or root will not be able to
     * su to other users.
     */
    goto out;
  }

  /* decrement count */
  count = pc_modify_login_count(pam_user, -1);
  if (count == -ESTALE) {
    count = 1;
  } else if (count < 0) {
    rc = PAM_SESSION_ERR;
    goto out;
  }

  /* check count */
  count = pc_modify_login_count(pam_user, 0);
  if (count == -ESTALE) {
    count = 1;
  } else if (count < 0) {
    rc = PAM_SESSION_ERR;
    goto out;
  }

  if (count == 0) {
    /* if uri is set to winbind_*, get the path from pam_winbind */
    if (strncmp(pc_config.uri, "winbind", 7) == 0) {
      if (strcmp(pc_config.uri, "winbind") == 0 ||
          strcmp(pc_config.uri, "winbind_profile") == 0) {
        val = pam_get_data(pamh, PAM_WINBIND_PROFILEPATH, &tmp);
      } else if (strcmp(pc_config.uri, "winbind_homedir") == 0) {
        val = pam_get_data(pamh, PAM_WINBIND_HOMEDIR, &tmp);
      } else {
        goto out;
      }

      if (val == PAM_NO_MODULE_DATA) {
        pc_log("ERROR: trying to get %s from winbind\n", pc_config.uri);
        goto out;
      }
      val = pc_winbind_uri(&pc_config.uri, tmp, !pc_config.krb5_set);
      pc_log("INFO: use pam_winbind support; server: %s\n", pc_config.uri);
      if (val < 0) {
        pc_log("ERROR: creating smb uri with winbind profile path\n");
        goto out;
      }
    }

    pc_log("INFO: expanding uri: %s ", pc_config.uri);
    pc_config.uri = c_strreplace(pc_config.uri, "%(USER)", pc_config.user);
    pc_log("to %s\n", pc_config.uri);
    if (pc_config.uri == NULL) {
      pc_log("ERROR: expanding user\n");
      goto out;
    }

    /* the config is still in memory */
    rc = _csync(pam_user, authtok, pam_home, pc_config.uri);
  }

out:
  if (pc_config.krb5_set) {
    unsetenv("KRB5CCNAME");
  }

  /* pc_config is automatically freed. PAM calls _cleanup_config(). */

  pc_leave_function_log(pamh, "pam_sm_close_session", rc);

  SAFE_FREE(pam_home);

  return rc;
}

/*
 * This is a placeholder function so PAM does not get mad.
 */
PAM_EXTERN int pam_sm_chauthtok(pam_handle_t *pamh, int flags,
    int argc, const char **argv) {
  return pam_sm_authenticate(pamh, flags, argc, argv);
}

#ifdef PAM_STATIC

struct pam_module _pam_csync_modstruct =
{
  .name                 = "pam_csync",
  .pam_sm_authenticate  = pam_sm_authenticate,
  .pam_sm_setcred       = pam_sm_setcred,
  .pam_sm_acct_mgmt     = pam_sm_acct_mgmt,
  .pam_sm_open_sesion   = pam_sm_open_session,
  .pam_sm_close_session = pam_sm_close_session,
  .pam_sm_chauthtok     = pam_sm_chauthtok,
};

#endif
