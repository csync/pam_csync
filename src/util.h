/*
 * pam_csync -- a module to support roaming home directories with csync
 *
 * Copyright (c) 2000      by Elvis Pfützenreuter <epx@conectiva.com>
 * Copyright (c) 2005-2008 by Jan Engelhardt
 * Copyright (c) 2005      by Bastian Kleineidam
 * Copyright (c) 2008      by Andreas Schneider <mail@cynapses.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * vim: ts=2 sw=2 et cindent
 */

/*
 * This file is mostly based on misc.h and private.h from pam_mount
 */

#ifndef _P_UTIL_H
#define _P_UTIL_H

#include <libgen.h>

#include "configuration.h"

extern unsigned int pc_debug;
extern unsigned int pc_silent;

#define PC_PREFIX       "pam_csync(%s:%u) "
#define pc_log(fmt, ...) \
  _pc_log((PC_PREFIX fmt), basename(__FILE__), \
    __LINE__, ## __VA_ARGS__)
#define pc_warn(fmt, ...) \
  _pc_warn((PC_PREFIX fmt), basename(__FILE__), \
    __LINE__, ## __VA_ARGS__)

/* GCC have printf type attribute check.  */
#ifdef __GNUC__
#define PRINTF_ATTRIBUTE(a,b) __attribute__ ((__format__ (__printf__, a, b)))
#else
#define PRINTF_ATTRIBUTE(a,b)
#endif /* __GNUC__ */

#define ASCIIZ_LLX sizeof("0xFFFFFFFF""FFFFFFFF")

#define PAM_CSYNC_VAR_RUN "/var/run/pam_csync"

extern void _pc_log(const char *format, ...) PRINTF_ATTRIBUTE(1, 2);
extern void _pc_warn(const char *format, ...) PRINTF_ATTRIBUTE(1, 2);

extern void pc_print_ids(const char *where);

extern void pc_setuid(const char *user);

extern int pc_modify_login_count(const char *user, long amount);

extern int pc_get_home(char **home, const char *user);

extern int pc_winbind_uri(char **uri, const char *profile_path, int pw);

extern int pc_user_excluded(config_t *c);

#endif /* _P_UTIL_H */
