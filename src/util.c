/*
 * pam_csync -- a module to support roaming home directories with csync
 *
 * Copyright (c) 2000      by Elvis Pfützenreuter <epx@conectiva.com>
 * Copyright (c) 2005-2008 by Jan Engelhardt
 * Copyright (c) 2005      by Bastian Kleineidam
 * Copyright (c) 2008      by Andreas Schneider <mail@cynapses.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * vim: ts=2 sw=2 et cindent
 */

/*
 * This file is mostly based on misc.c and pmvarrun.c from pam_mount
 */

#include <errno.h>
#include <assert.h>
#include <limits.h>
#include <fnmatch.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <pwd.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>


#include <security/pam_modules.h>
#include <security/pam_modutil.h>
#include <security/pam_ext.h>

#include "cstdlib.h"
#include "util.h"

#define PAM_CSYNCTAB "/etc/security/pam_csynctab"

unsigned int pc_debug;
unsigned int pc_silent;

/* logging function */
void _pc_log(const char *format, ...) {
  va_list args, arg2;

  assert(format != NULL);

  if (pc_silent) {
    return;
  }

  va_start(args, format);
  va_copy(arg2, args);
  if (pc_debug) {
    vfprintf(stderr, format, args);
  }
  vsyslog(LOG_AUTH | LOG_ERR, format, arg2);
  va_end(args);
  va_end(arg2);

  return;
}

void _pc_warn(const char *format, ...) {
  va_list args, arg2;

  assert(format != NULL);
  if (pc_debug == 0) {
    return;
  }

  va_start(args, format);
  va_copy(arg2, args);
  vfprintf(stderr, format, args);
  vsyslog(LOG_AUTH | LOG_ERR, format, arg2);
  va_end(args);
  va_end(arg2);

  return;
}

/**
 * _create_var_run
 *
 * Creates the /var/run/pam_csync directory required by pmvarrun and sets
 * proper permissions on it.
 *
 * Returns >0 for success or <=0 to indicate errno.
 */
static int _create_var_run(void) {
  int ret;

  pc_warn("creating " PAM_CSYNC_VAR_RUN "\n");
  if (mkdir(PAM_CSYNC_VAR_RUN, 0000) < 0) {
    ret = -errno;
    pc_log("unable to create " PAM_CSYNC_VAR_RUN ": %s\n", strerror(errno));
    return ret;
  }
  if (chown(PAM_CSYNC_VAR_RUN, 0, 0) < 0) {
    ret = -errno;
    pc_log("unable to chown " PAM_CSYNC_VAR_RUN ": %s\n", strerror(errno));
    return ret;
  }

  /*
   * 0755: `su` creates file group owned by user and then releases root
   * permissions. User needs to be able to access file on logout.
   */
  if (chmod(PAM_CSYNC_VAR_RUN, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH) < 0) {
    ret = -errno;
    pc_log("unable to chmod " PAM_CSYNC_VAR_RUN ": %s\n", strerror(errno));
    return ret;
  }

  return 1;
}

/**
 * _open_and_lock
 *
 * @filename:  file to open
 *
 * Creates if necessary, opens and chown()s @filename, and locks it.
 * Returns the fd if all of that succeeded, -EAGAIN if the file was unlinked
 * during operation (see below), -ESTALE if the lock could not be obtained,
 * and <0 otherwise to indicate errno.
 */
static int _open_and_lock(const char *filename, long uid) {
  struct flock lockinfo = {
    .l_type   = F_WRLCK,
    .l_whence = SEEK_SET,
    .l_start  = 0,
    .l_len    = 0,
  };
  struct stat sb;
  int fd, ret;

  if ((fd = open(filename, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR)) < 0) {
    ret = -errno;
    pc_log("unable to open %s: %s\n", filename, strerror(errno));
    return ret;
  }
  if (fchown(fd, uid, 0) < 0) {
    ret = -errno;
    pc_log("unable to chown %s: %s\n", filename, strerror(errno));
    return ret;
  }

  /*
   * Note: Waiting too long might interfere with LOGIN_TIMEOUT from
   * /etc/login.defs, and /bin/login itself may prematurely kill the
   * /session.
   */
  alarm(20);
  ret = fcntl(fd, F_SETLKW, &lockinfo);
  alarm(0);
  if (ret == EAGAIN) {
    /*
     * [Flyn] If someone has locked the file and not written to it
     * in at least 20 seconds, we assume they either forgot to
     * unlock it or are catatonic -- chances are slim that they are
     * in the middle of a read-write cycle and I do not want to
     * make us lock users out. Perhaps I should just return
     * %PAM_SUCCESS instead and log the event? Kill the process
     * holding the lock? Options abound... For now, we ignore it.
     *
     * [CCJ] pmvarrun is the only one ever writing to that file,
     * and we keep the lock as short as possible. So if there is no
     * response within the time limit, something is fouled up (e.g. 
     * NFS server not responding -- though /var/run should at best
     * not be on an NFS mount).  Continue, let user log in, do not
     * change anything.
     */
    pc_warn("stale lock on file %s - continuing without increasing"
        "pam_csync reference count\n", filename);
    close(fd);
    return -ESTALE;
  }

  /*
   * It is possible at this point that the file has been removed by a
   * previous login; if this happens, we need to start over.
   */
  if (stat(filename, &sb) < 0) {
    ret = -errno;
    close(fd);
    if (ret == -ENOENT)
      return -EAGAIN;
    return ret;
  }

  return fd;
}

/**
 * _read_current_count
 *
 * @fd:   file descriptor to read from
 *
 * Reads the current user's reference count from @fd and returns the value
 * on success. Otherwise, returns -EOVERFLOW in case we suspect a problem or
 * <0 to indicate errno.
 */
static long _read_current_count(int fd, const char *filename) {
  char buf[ASCIIZ_LLX] = {0};
  long ret;

  if ((ret = read(fd, buf, sizeof(buf))) < 0) {
    ret = -errno;
    pc_log("read error on %s: %s\n", filename, strerror(errno));
    close(fd);
    return ret;
  } else if (ret == 0) {
    /* File is empty, ret is already 0 -- we are set. */
  } else if ((unsigned long) ret < sizeof(buf)) {
    char *p = NULL;
    if ((ret = strtol(buf, &p, 0)) >= LONG_MAX || p == buf) {
      pc_log("parse problem / session count corrupt "
          "(overflow), check your refcount file\n");
      return -EOVERFLOW;
    }
  } else if ((unsigned long) ret >= sizeof(buf)) {
    pc_log("session count corrupt (overflow)\n");
    return -EOVERFLOW;
  }

  return ret;
}

/**
 * _write_count
 *
 * @fd:         file descriptor to write to
 * @nv:         new value to write
 * @filename:   filename, only used for pc_log()
 *
 * Writes @nv as a number in hexadecimal to the start of the file @fd and
 * truncates the file to the written length.
 */
static int _write_count(int fd, long nv, const char *filename) {
  char buf[ASCIIZ_LLX];
  int wrt, len, ret;

  if (nv <= 0) {
    if (unlink(filename) >= 0)
      return 1;
    if (errno != EPERM)
      pc_log("could not unlink %s: %s\n", filename, strerror(errno));
    /*
     * Fallback to just blanking the file. This can happen when
     * pmvarrun is called as unprivileged user.
     */
    if (ftruncate(fd, 0) < 0)
      pc_warn("truncate failed: %s\n", strerror(errno));
    return 1;
  }

  if ((ret = lseek(fd, 0, SEEK_SET)) != 0) {
    ret = -errno;
    pc_log("failed to seek in %s: %s\n", filename, strerror(errno));
    return ret;
  }

  len = snprintf(buf, sizeof(buf), "0x%lX", nv);
  if ((wrt = write(fd, buf, len)) != len) {
    ret = -errno;
    pc_log("wrote %d of %d bytes; write error on %s: %s\n",
        (wrt < 0) ? 0 : wrt, len, filename, strerror(errno));
    return ret;
  }

  if (ftruncate(fd, len) < 0) {
    ret = -errno;
    pc_log("truncate failed: %s\n", strerror(errno));
    return ret;
  }

  return 1;
}
void pc_print_ids(const char *where) {
  pc_warn("%s: (uid=%u, euid=%u, gid=%u, egid=%u)\n", where,
      (unsigned int) getuid(),
      (unsigned int) geteuid(),
      (unsigned int) getgid(),
      (unsigned int) getegid());

  return;
}

/* Set UID and GID to the user's one */
void pc_setuid(const char *user) {
  const struct passwd *real_user;

  pc_warn("setting uid to user %s\n", user);

  if ((real_user = getpwnam(user)) == NULL) {
    pc_log("could not get passwd entry for user %s\n", user);
    return;
  }

  if (setgid(real_user->pw_gid) == -1) {
    pc_log("could not set gid to %ld\n",
        (long) real_user->pw_gid);
    return;
  }

  if (setuid(real_user->pw_uid) == -1) {
    pc_log("could not set uid to %ld\n",
        (long) real_user->pw_uid);
    return;
  }

  setenv("HOME", real_user->pw_dir, 1);
  setenv("USER", real_user->pw_name, 1);

  pc_print_ids("pc_setuid");

  return;
}

/*
 * @user:    user to poke on
 * @amount:  increment (usually -1, 0 or +1)
 *
 * Adjusts /var/run/pam_csync/@user by @amount, or deletes the file if the
 * resulting value (current + @amount) is <= 0. Returns >= 0 on success to
 * indicate the new login count, or negative to indicate errno. -ESTALE and
 * -EOVERFLOW are passed up from subfunctions and must be handled in the
 * caller.
 */
int pc_modify_login_count(const char *user, long amount) {
  char filename[PATH_MAX + 1];
  struct passwd *pent;
  struct stat sb;
  int fd, ret;
  long val;

  assert(user != NULL);

  if ((pent = getpwnam(user)) == NULL) {
    ret = -errno;
    pc_log("could not resolve user %s\n", user);
    return ret;
  }

  if (stat(PAM_CSYNC_VAR_RUN, &sb) < 0) {
    if (errno != ENOENT) {
      ret = -errno;
      pc_log("unable to stat " PAM_CSYNC_VAR_RUN ": %s\n",
          strerror(errno));
      return ret;
    }
    if ((ret = _create_var_run()) < 0)
      return ret;
  }

  snprintf(filename, sizeof(filename), PAM_CSYNC_VAR_RUN "/%s", user);
  while ((ret = fd = _open_and_lock(filename, pent->pw_uid)) == -EAGAIN)
    /* noop */;
  if (ret < 0)
    return ret;

  if ((val = _read_current_count(fd, filename)) < 0) {
    close(fd);
    return val;
  }

  pc_warn("parsed count value %ld\n", val);
  /* amount == 0 implies query */
  ret = 1;
  if (amount != 0)
    ret = _write_count(fd, val + amount, filename);

  close(fd);
  return (ret < 0) ? ret : val + amount;
}

int pc_get_home(char **home, const char *user) {
  struct passwd *pe = NULL;
  char *buf = NULL;

  if ((pe = getpwnam(user)) == NULL) {
    pc_log("Could not lookup account information for %s\n", user);
    return PAM_SYSTEM_ERR;
  }

  buf = c_strdup(pe->pw_dir);
  if (buf == NULL) {
    pc_log("%s\n", strerror(errno));
    return PAM_SYSTEM_ERR;
  }

  *home = buf;

  return PAM_SUCCESS;
}

int pc_winbind_uri(char **uri, const char *profile_path, int pw) {
  static FILE *csynctab;
  const char *token = NULL;
  char line[1024] = {0};
  char *replace = NULL;
  char *temp = NULL;
  char *new = NULL;
  char *pp = NULL;
  char *p = NULL;
  size_t len = 0;
  int rc = -1;

  pp = c_strdup(profile_path);
  if (pp == NULL) {
    *uri = NULL;
    return -1;
  }

  /* replace '\' with '/' */
  while ((p = strchr(pp, '\\')) != NULL) *p = '/';

  csynctab = fopen(PAM_CSYNCTAB, "r");

  if (csynctab != NULL)  {
    while (fgets (line, sizeof(line), csynctab ) != NULL) {
      /* skip comment lines and newlines */
      if (line[0] == '#' || line[0] == '\n') {
        continue;
      }

      /* split line at first | */
      token = strtok (line, "|");
      if (token == NULL) {
        continue;
      }

      if (strncmp(pp, token, strlen(token)) == 0) { /* if uri = first part */
        replace = c_strdup(token);
        if (replace == NULL) {
          free(pp);
          fclose(csynctab);
          return -1;
        }

        token = strtok(NULL, "|");  /* get second part */
        if (token == NULL) {
          continue;
        }

        /* chop linefeed */
        if ((temp = strchr(token, '\n'))) {
          *temp = '\0';
        }

        p = c_strreplace(pp, replace, token);
        pc_log("INFO: new path value from " PAM_CSYNCTAB " -> %s\n", p);
        break;
      } else {
        p = pp;
      }
    }
    fclose(csynctab);
  } else {
    p = pp;
  }

  /* remove leading slashes */
  while (*p == '/') p++;

  len = 6 + strlen(p) + 1; /* smb:// + p + \0 */
  if (pw) len += 16; /* %(USER):%(PASS)@ = 6 */

  new = (char *) c_malloc(len);
  if (new == NULL) {
    *uri = NULL;
    rc = -1;
    goto out;
  }

  snprintf(new, len, "smb://%s%s", pw ? "%(USER):%(PASS)@" : "", p);

  SAFE_FREE(*uri);
  *uri = new;
  rc = 0;
out:
  free(pp);

  return rc;
}

int pc_user_excluded(config_t *c) {
  int i = 0;

  for (i = 0; i < c->exclude.count; i++) {
    if (fnmatch(c->exclude.vector[i], c->user, 0) == 0) {
      return 1;
    }
  }
  return 0;
}

