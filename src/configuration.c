/*
 * pam_csync -- a module to support roaming home directories with csync
 *
 * Copyright (c) 2008      by Andreas Schneider <mail@cynapses.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 * vim: ts=2 sw=2 et cindent
 */

#include <sys/mman.h>
#include <iniparser.h>

#include "configuration.h"
#include "cstdlib.h"

void config_init(config_t *c) {
  c->krb5_set = 0;
  c->uri = NULL;
  c->user = NULL;
  c->msg_authpw = c_strdup("pam_csync password: ");
  c->msg_sessionpw = c_strdup("re-enter pam_csync password: ");
  c->exclude.vector = NULL;
  c->exclude.count = 0;
}

int config_load(const char *f, config_t *c) {
  dictionary *d = NULL;
  const char *exclude = NULL;
  char *tmp = NULL;
  int i = 0;

  d = iniparser_load(f);
  if (d == NULL) {
    return -1;
  }

  c->uri = c_strdup(iniparser_getstring(d, "global:uri", NULL));

  if (iniparser_getstring(d, "global:msg_authpw", NULL) != NULL) {
    SAFE_FREE(c->msg_authpw);
    c->msg_authpw = c_strdup(iniparser_getstring(d, "global:msg_authpw", NULL));
  }

  if (iniparser_getstring(d, "global:msg_sessionpw", NULL) != NULL) {
    SAFE_FREE(c->msg_sessionpw);
    c->msg_sessionpw = c_strdup(iniparser_getstring(d, "global:msg_sessionpw", NULL));
  }

  exclude = iniparser_getstring(d, "global:exclude", NULL);
  if (exclude != NULL) {
    tmp = strtok((char *)exclude, ",");
    while (tmp != NULL) {
      i++;
      while (*tmp == ' ') tmp++;
      c->exclude.vector = (char **) c_realloc(c->exclude.vector, i * sizeof(char *));
      c->exclude.vector[i - 1] = c_strdup(tmp);
      tmp = strtok(NULL, ",");
    }
    c->exclude.count = i;
  }

  if (d) {
    iniparser_freedict(d);
  }

  return 0;
}

void config_free(config_t *c) {
  int i = 0;
  if (c != NULL) {
    SAFE_FREE(c->uri);
    SAFE_FREE(c->msg_authpw);
    SAFE_FREE(c->msg_sessionpw);
  }

  for (i = 0; i < c->exclude.count; i++) {
    SAFE_FREE(c->exclude.vector[i]);
  }
  SAFE_FREE(c->exclude.vector);

  return;
}

